osu! IRC bot for the qualifiers stage of the [National Dutch Championship]

This bot has been used for the qualifiers of the following tournaments:
- NBB 2022
- NBB 2023
- NBB 2024
- PNK 2022
- PNK 2023
- NDC 2023 (badged)

In this version, players will schedule their lobby on our website at least one hour in advance. The bot will create a lobby for them and send an invite when their lobby is scheduled to start. Players can choose the order of beatmaps themselves. Every map can be played only once. Leaving the lobby is allowed once, but only if they return within 2 minutes. All lobbies are monitored by a human referee at all times.

## How to run
- Make sure you have [NodeJS] and npm installed (npm comes with Node)
- Clone this repository to your pc
- Run `npm install` to install the dependencies
- Add a `.env` file in the root folder and fill it with the required environment variables (template below)
- Start the bot with `npm start`

## Environment variables template
this is outdated lol
```
IRC_SERVER=
IRC_PORT=
IRC_USERNAME=
IRC_PASSWORD=

BOT_PREFIX=!

OSU_API_KEY=
```

## Licensing
You are allowed to with this whatever you want, as long as you properly credit me ([Mr HeliX])

[National Dutch Championship]: <https://tourney.huismetbenen.nl/25>
[NodeJS]: <https://nodejs.org>
[Mr HeliX]: <https://osu.ppy.sh/u/2330619>