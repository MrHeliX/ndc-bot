import dotenv from "dotenv";
import { handleBanchoBotMessage, handleIncomingCommands } from "./commands/commands-handler";
import { initializeDbConnection } from "./services/db";
import { setStateValue } from "./state/state";
import { Client } from "irc-framework";
import { getCurrentQualifiersLobbies, initializeScheduledLobbyChecker } from "./commands/results/qualifiers";
import { getMappoolFromDb } from "./state/mappool";
import fs from "fs";
import moment from "moment";

const startBot = async (): Promise<void> => {
    dotenv.config();

    await initializeDbConnection();
    await getMappoolFromDb();
    
    const { IRC_SERVER, IRC_USERNAME, IRC_PASSWORD } = process.env;

    const client = new Client();
    client.connect({
        host: IRC_SERVER,
        port: 6667,
        nick: IRC_USERNAME,
        password: IRC_PASSWORD
    });

    setStateValue("client", client);

    client.on("registered", async () => {
        console.log("Connected!");

        // Get all active lobbies so these can be continued
        await getCurrentQualifiersLobbies();
        await initializeScheduledLobbyChecker();
    });

    client.on("message", event => {
        console.log("message event", event);
        const { nick, message, target } = event;

        logAllChatMessages(target, nick, message);

        if (nick === "BanchoBot")
            handleBanchoBotMessage(message, target);
        else
            handleIncomingCommands(nick, message, target);
    });

    client.on("join", event => {

    });

    client.on("error", message => {
        console.log("error");
        console.log(message);
    });
};

export const logAllChatMessages = (channel: string, nick: string, message: string) => {
    const fileName = channel.startsWith("#") ? `${channel}.txt` : `${nick}.txt`;
    const now = moment.utc();
    fs.appendFileSync(`${__dirname}/../logs/${fileName}`, `${now.format("YYYY-MM-DD HH:mm:ss")} | ${nick}: ${message}\r\n`);
};

startBot();