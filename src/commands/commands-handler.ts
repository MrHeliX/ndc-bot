import { dbGetBeatmapFromQualifiersPool, dbGetQualifiersLobbyByUserId, dbMarkQualifiersLobbyAsClosed, dbMarkQualifiersLobbyAsStarted } from "../helpers/db-queries";
import { sendWebhookMessage } from "../helpers/discord-webhook-helpers";
import { parseUsernameToIRC } from "../helpers/generic-helpers";
import { BotCommand, CommandResponse } from "../interfaces/generic-interfaces";
import { QualifiersLobby } from "../qualifiers/QualifiersLobby";
import { osuGetPlayer } from "../services/osu/user";
import { addQualifiersLobby, getLobbyFromStateByChannel } from "../state/lobbies";
import { getStateValue, isUserBusy, setUserAsAvailable, setUserAsBusy } from "../state/state";
import { getUserFromStateByName } from "../state/users";
import { getCommand, returnMessage } from "./commands";
import { sendResponse } from "./responses-handler";

export const handleIncomingCommands = async (username: string, message: string, channel: string): Promise<void> => {
    const [command, ...args] = message.split(" ");
    if (!command.startsWith("!")) return;

    if (isUserBusy(username)) {
        console.log(`BUSY: ${username}`);
        return;
    }
    else
        console.log(`NOT BUSY: ${username}`);

    setUserAsBusy(username);

    const trigger = command.split("!")[1];

    const userFromState = getUserFromStateByName(username);

    const user = userFromState ?? await osuGetPlayer(username);
    const user_id = +user.user_id;

    const botCommand: BotCommand = { user_id, username, trigger, args, channel };

    const response = await handleCommand(botCommand);
    if (response)
        sendResponse(response, user_id);
    else
        setUserAsAvailable(username);
};

export const handleBanchoBotMessage = async (message: string, channel: string): Promise<void> => {
    if (message.startsWith("Created the tournament match")) {
        // Parse lobby id and username
        const mpLink = message.match("https://osu.ppy.sh/mp/[0-9]*")?.[0];
        const username = message.match(new RegExp("Qualifiers [\\w *\\[\\]\\-]*"))?.[0]?.split("Qualifiers")[1]?.trim();
        const lobbyId = +(mpLink?.split("/").pop());

        const user = getUserFromStateByName(username);
        console.log(user);

        const dbLobby = await dbGetQualifiersLobbyByUserId(user.user_id);
        if (!dbLobby) {
            sendWebhookMessage(`Kon niet de geplande lobby vinden voor ${username} (${user.user_id})`);
            return;
        }

        await dbMarkQualifiersLobbyAsStarted(dbLobby.lobby_id, mpLink, lobbyId);
        const lobby = new QualifiersLobby(lobbyId, user, mpLink, dbLobby.lobby_id);
        addQualifiersLobby(lobby);

        // Set scorev2 and invite user to lobby
        const responses = await lobby.setLobbySettingsAndSendInvite();
        responses.push(returnMessage(`Je lobby staat klaar; als je geen invite hebt gehad, gebruik dan het "!invite" commando`, user.username));
        sendResponse(responses, user.user_id);

        console.log(getStateValue("lobbies"));
    }

    if (message.startsWith("Changed beatmap to")) {
        const beatmapUrl = message.match("https://osu.ppy.sh/b/[0-9]*")[0];
        const mapId = +(beatmapUrl?.split("/").pop());
        const beatmap = await dbGetBeatmapFromQualifiersPool(mapId);

        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.onMapChange(beatmap);
    }

    if (message.startsWith("The match has started!")) {
        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.onMapStart();
    }

    if (message.startsWith("The match has finished!")) {
        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.checkForNewScores();
    }

    if (message === "All players are ready") {
        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.startMap();
    }

    if (message.endsWith("joined in slot 1.")) {
        const username = parseUsernameToIRC(message.split(" joined in slot 1.")[0]);
        const user = getUserFromStateByName(username);

        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.onLobbyJoin(user);
    }

    if (message.startsWith("Beatmap: https://osu.ppy.sh/b/")) {
        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.checkIfCorrectMapSelected(message);
    }

    if (message.startsWith("Active mods: ")) {
        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.checkIfCorrectModsSelected(message);
    }

    if (message.startsWith("Slot 1")) {
        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.checkIfCorrectPlayer(message);
    }

    if (message.endsWith("left the game.")) {
        const username = parseUsernameToIRC(message.split(" left the game.")[0]);
        const user = getUserFromStateByName(username);

        const lobby = getLobbyFromStateByChannel(channel);
        lobby?.onLobbyLeave(user);
    }

    // Should not be necessary
    if (message.startsWith("Closed the match")) {
        const lobby = getLobbyFromStateByChannel(channel);
        if (lobby)
            await dbMarkQualifiersLobbyAsClosed(lobby.db_lobby_id);
    }
};

const handleCommand = async (botCommand: BotCommand): Promise<CommandResponse[] | void> => {
    const command = getCommand(botCommand.trigger);
    if (!command) return;
    return await command.result(botCommand);
};