import { BotCommand, Command, CommandResponse } from "../interfaces/generic-interfaces";
import { calmDownQualifiersLobby, closeQualifiersLobby, getInviteForQualifiersLobby, getQualifiersLobbyStatus, panicQualfiersLobby, requestRefereeForQualifiersLobby, selectQualifiersMap, startQualifiersMap, startQualifiersRequest } from "./results/qualifiers";

const commands: Command[] = [
    { trigger: "ping", result: ({ username }: BotCommand) => [returnMessage("Pong!", username)] },
    { trigger: "q", result: startQualifiersRequest },
    { trigger: "map", result: selectQualifiersMap },
    { trigger: "status", result: getQualifiersLobbyStatus },
    { trigger: "start", result: startQualifiersMap },
    { trigger: "invite", result: getInviteForQualifiersLobby },
    { trigger: "close", result: closeQualifiersLobby },
    { trigger: "req-ref", result: requestRefereeForQualifiersLobby },
    { trigger: "panic", result: panicQualfiersLobby },
    { trigger: "calmdown", result: calmDownQualifiersLobby }
];

export const getCommand = (trigger: string): Command | undefined => {
    return commands.find(c => c.trigger === trigger);
};

export const returnMessage = (message: string, user: string): CommandResponse => ({ type: "MESSAGE", message, user });

export const returnAction = (message: string, user: string): CommandResponse => ({ type: "ACTION", message, user });
