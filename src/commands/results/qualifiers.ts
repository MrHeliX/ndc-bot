import { dbGetActiveQualifiersLobbiesForBot, dbGetActiveQualifiersLobbyByUserId, dbGetBeatmapFromQualifiersPoolByModBracket, dbGetHostByUserId, dbGetRegistrationByUserId, dbGetScheduledLobbiesForBot, dbGetScheduledLobbyForUser, dbGetScoresForPlayerOnMap } from "../../helpers/db-queries";
import { sendWebhookMessage } from "../../helpers/discord-webhook-helpers";
import { parseUsernameToIRC } from "../../helpers/generic-helpers";
import { createPrivateMultiplayerLobby } from "../../helpers/mp-commands";
import { ModBracket } from "../../interfaces/db/beatmaps-interfaces";
import { BotCommand, CommandResponse } from "../../interfaces/generic-interfaces";
import { QualifiersLobby } from "../../qualifiers/QualifiersLobby";
import { addQualifiersLobby, getLobbyFromStateByChannel, getLobbyFromStateByPlayer } from "../../state/lobbies";
import { getStateValue } from "../../state/state";
import { returnMessage } from "../commands";
import { sendResponse } from "../responses-handler";

export const initializeScheduledLobbyChecker = async (): Promise<void> => {
    await checkScheduledLobbies();
    global.setInterval(checkScheduledLobbies, 60000);
};

const checkScheduledLobbies = async () => {
    const lobbies = await dbGetScheduledLobbiesForBot();
    console.log(lobbies);

    lobbies.forEach(l => {
        sendResponse([returnMessage("Hoi, je qualifiers lobby staat voor nu ingepland. Stuur binnen 10 minuten het commando \"!q\" om te starten!", parseUsernameToIRC(l.username))]);
        sendWebhookMessage(`Lobby voor ${l.username} (${l.user_id}) staat nu ingepland; gevraagd om bevestiging`);
    });
};

export const getCurrentQualifiersLobbies = async (): Promise<void> => {
    const lobbies = await dbGetActiveQualifiersLobbiesForBot();
    const client = getStateValue("client");

    console.log(lobbies);

    lobbies.forEach(l => {
        const lobby = new QualifiersLobby(l.osu_lobby_id, { user_id: l.user_id, username: l.username }, l.mp_link, l.lobby_id);
        addQualifiersLobby(lobby);

        client.join(`#mp_${l.osu_lobby_id}`);
    });
};

export const startQualifiersRequest = async (command: BotCommand): Promise<CommandResponse[]> => {
    const registration = await dbGetRegistrationByUserId(command.user_id);

    if (!registration)
        return [returnMessage("Je bent niet ingeschreven voor dit toernooi!", command.username)];

    const scheduledLobby = await dbGetScheduledLobbyForUser(command.user_id);
    if (!scheduledLobby)
        return [returnMessage("Er staat op dit moment geen lobby voor je ingepland. Mocht je te laat zijn en je lobby gemist hebben, neem dan contact op met Mr HeliX", command.username)];

    // TODO: more than 4 lobbies handler

    sendWebhookMessage(`${command.username} (${command.user_id}) heeft de lobby bevestigd; lobby wordt aangemaakt...`);
    createPrivateMultiplayerLobby(registration);

    return [returnMessage("", command.username)];
};

export const selectQualifiersMap = async (command: BotCommand): Promise<CommandResponse[]> => {
    if (!command.channel.startsWith("#mp_"))
        return;
    if (!command.args?.length)
        return [returnMessage("Je hebt niet aangegeven welke map je wilt spelen (bijvoorbeeld !map NM1 of !map HD2", command.channel)];

    const modBracket = command.args[0].substring(0, 2).toUpperCase();
    const modBracketIndex = +(command.args[0].substring(2, 3));

    if (!["NM", "HD", "HR", "DT"].includes(modBracket) || isNaN(modBracketIndex))
        return [returnMessage("Die map bestaat niet", command.channel)];

    const beatmap = await dbGetBeatmapFromQualifiersPoolByModBracket(modBracket as ModBracket, modBracketIndex);

    if (!beatmap)
        return [returnMessage("Die map bestaat niet", command.channel)];

    const existingScores = await dbGetScoresForPlayerOnMap(command.user_id, beatmap.uid);
    if (existingScores.length)
        return [returnMessage("Je hebt deze map al een keer gespeeld! Kies een andere map", command.channel)];

    const mods = getModsToSelect(modBracket as ModBracket);

    const lobby = getLobbyFromStateByChannel(command.channel);
    const responses = lobby?.selectMap(beatmap, mods.join(" "));
    
    return responses;
};

export const getModsToSelect = (modBracket: ModBracket): string[] => {
    if (modBracket === "NM") return ["NF"];
    return [modBracket, "NF"];
};

export const startQualifiersMap = async (command: BotCommand): Promise<CommandResponse[]> => {
    return;
    // const lobby = getLobbyFromStateByChannel(command.channel);
    // const responses = lobby?.startMap();

    // return responses;
};

export const getQualifiersLobbyStatus = async (command: BotCommand): Promise<CommandResponse[]> => {
    const lobby = getLobbyFromStateByChannel(command.channel);
    const responses = lobby?.logLobbyStatus();

    return responses ?? [];
};

export const getInviteForQualifiersLobby = async (command: BotCommand): Promise<CommandResponse[]> => {
    const activeLobby = await dbGetActiveQualifiersLobbyByUserId(command.user_id);
    if (activeLobby) {
        const lobby = getLobbyFromStateByPlayer(command.user_id);
        return lobby?.invitePlayer();
    }
    else
        return [returnMessage(`Je hebt geen actieve qualifiers lobby`, command.username)];
};

export const closeQualifiersLobby = async (command: BotCommand): Promise<CommandResponse[]> => {
    const lobby = getLobbyFromStateByChannel(command.channel);
    return lobby?.closeLobbyRequest();
};

export const requestRefereeForQualifiersLobby = async (command: BotCommand): Promise<CommandResponse[]> => {
    if (!command.args?.length)
        return [returnMessage(`Geef de user id als parameter mee`, command.username)];

    const host = await dbGetHostByUserId(command.user_id);
    if (!!host) {
        const player_id = +command.args[0];
        if (isNaN(player_id))
            return [returnMessage(`Geen geldige user id`, command.username)];

        const lobby = getLobbyFromStateByPlayer(player_id);
        if (!lobby)
            return [returnMessage(`Geen lobby gevonden voor deze speler: ${player_id}`, command.username)];

        return [
            ...(lobby?.addReferees([host.user_id]) ?? []),
            returnMessage(`Je wordt toegevoegd als referee voor deze qualifiers lobby. Join over een paar seconden de volgende channel in een IRC client: ${lobby.channel}`, command.username)
        ];
    }
    else {
        sendWebhookMessage(`${command.username} (${command.user_id}) probeerde de lobby te kapen maar is geen host`);
    }
};

export const panicQualfiersLobby = async (command: BotCommand): Promise<CommandResponse[]> => {
    const lobby = getLobbyFromStateByChannel(command.channel);
    lobby.in_panic_mode = true;

    sendWebhookMessage(`<@95939949009866752>: ${command.username} (${command.user_id}) is in paniek! Doe iets aub!!`);
    return [returnMessage("Paniek in de lobby!", command.username)];
};

export const calmDownQualifiersLobby = async (command: BotCommand): Promise<CommandResponse[]> => {
    const host = await dbGetHostByUserId(command.user_id);
    if (!host) return [returnMessage("Alleen organisatie kan de lobby kalmeren", command.username)];
    
    const lobby = getLobbyFromStateByChannel(command.channel);
    lobby.in_panic_mode = false;

    sendWebhookMessage(`${command.username}'s lobby is weer rustig!`);
    return [returnMessage("Rust in de lobby!", command.username)];
};
