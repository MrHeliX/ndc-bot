import moment from "moment";
import { Beatmap, ModBracket } from "../interfaces/db/beatmaps-interfaces";
import { IQualifiersLobby, QualifiersScore } from "../interfaces/db/qualifiers-interfaces";
import { Registration } from "../interfaces/db/registrations-interfaces";
import { executeSqlQuery, executeSqlQueryFirstRow, executeSqlSingleInsert } from "../services/db";

export const dbGetBeatmapsInQualifiersPool = async (): Promise<Beatmap[]> => {
    return await executeSqlQuery<Beatmap>(`
        SELECT b.* FROM beatmaps b
        INNER JOIN mappools m ON b.mappool_id = m.id
        INNER JOIN tourney_rounds tr ON m.round_uid = tr.uid
        WHERE tr.round_id = "qualifiers" AND tr.tourney_id = 7
    `);
};

export const dbGetBeatmapFromQualifiersPool = async (map_id: number): Promise<Beatmap> => {
    return await executeSqlQueryFirstRow<Beatmap>(`
        SELECT b.* FROM beatmaps b
        INNER JOIN mappools m ON b.mappool_id = m.id
        INNER JOIN tourney_rounds tr ON m.round_uid = tr.uid
        WHERE tr.round_id = "qualifiers" AND tr.tourney_id = 7 AND b.map_id = ?;
    `, [map_id]);
};

export const dbGetBeatmapFromQualifiersPoolByModBracket = async (mod_bracket: ModBracket, index: number): Promise<Beatmap> => {
    return await executeSqlQueryFirstRow<Beatmap>(`
        SELECT b.* FROM beatmaps b
        INNER JOIN mappools m ON b.mappool_id = m.id
        INNER JOIN tourney_rounds tr ON m.round_uid = tr.uid
        WHERE tr.round_id = "qualifiers" AND tr.tourney_id = 7 AND b.mod_bracket = ? AND b.mod_bracket_order = ?;
    `, [mod_bracket, index]);
};

export const dbGetScheduledLobbiesForBot = async (): Promise<(IQualifiersLobby & { username: string })[]> => {
    const currentTime = moment.utc().format("YYYY-MM-DD HH:mm");
    return await executeSqlQuery<IQualifiersLobby & { username: string }>(`
        SELECT l.*, u.user_id, u.username FROM qualifiers_lobbies l
        INNER JOIN registrations r ON l.lobby_id = r.qualifiers_lobby
        INNER JOIN doc.users u ON r.user_id = u.user_id
        WHERE l.tourney_id = 7 AND l.is_active = FALSE AND l.referee = ? AND l.lobby_date = ?;
    `, [+process.env.BOT_USER_ID, currentTime]);
};

export const dbGetScheduledLobbyForUser = async (user_id: number): Promise<IQualifiersLobby> => {
    const currentTime = moment.utc();
    const maxAllowedTime = currentTime.clone().subtract(10, "minutes");

    return await executeSqlQueryFirstRow<IQualifiersLobby>(`
        SELECT l.*, r.user_id FROM qualifiers_lobbies l
        INNER JOIN registrations r ON l.lobby_id = r.qualifiers_lobby
        WHERE l.tourney_id = 7 AND r.user_id = ? AND l.is_active = FALSE AND l.referee = ? AND l.lobby_date >= ? AND l.lobby_date <= ?;
    `, [user_id, +process.env.BOT_USER_ID, maxAllowedTime.format("YYYY-MM-DD HH:mm"), currentTime.format("YYYY-MM-DD HH:mm")]);
};

export const dbGetActiveQualifiersLobbiesForBot = async (): Promise<(IQualifiersLobby & { username: string })[]> => {
    return await executeSqlQuery<IQualifiersLobby & { username: string }>(`
        SELECT l.*, u.user_id, u.username FROM qualifiers_lobbies l
        INNER JOIN registrations r ON l.lobby_id = r.qualifiers_lobby
        INNER JOIN doc.users u ON r.user_id = u.user_id
        WHERE l.tourney_id = 7 AND l.is_active = TRUE AND l.referee = ?;
    `, [+process.env.BOT_USER_ID]);
};

export const dbGetQualifiersLobbyByUserId = async (user_id: number): Promise<IQualifiersLobby> => {
    return await executeSqlQueryFirstRow<IQualifiersLobby>(`
        SELECT l.* FROM qualifiers_lobbies l
        INNER JOIN registrations r ON l.lobby_id = r.qualifiers_lobby
        WHERE l.tourney_id = 7 AND r.user_id = ?;
    `, [user_id]);
};

export const dbGetActiveQualifiersLobbyByUserId = async (user_id: number): Promise<IQualifiersLobby> => {
    return await executeSqlQueryFirstRow<IQualifiersLobby>(`
        SELECT l.*, r.user_id FROM qualifiers_lobbies l
        INNER JOIN registrations r ON l.lobby_id = r.qualifiers_lobby
        WHERE r.user_id = ? AND l.tourney_id = 7 AND l.is_active = TRUE;
    `, [user_id]);
};

export const dbGetCurrentBeatmapInLobby = async (lobby_id: number): Promise<Beatmap> => {
    return await executeSqlQueryFirstRow<Beatmap>(`
        SELECT b.* FROM qualifiers_lobbies l
        INNER JOIN beatmaps b ON l.lobby_id = ? AND l.current_map = b.uid;
    `, [lobby_id])
};

export const dbGetScoresForPlayerOnMap = async (player_id: number, map_uid: number): Promise<QualifiersScore[]> => {
    return await executeSqlQuery<QualifiersScore>(`
        SELECT s.* FROM qualifiers_scores s
        WHERE s.player_id = ? AND s.map_uid = ?;
    `, [player_id, map_uid]);
};

export const dbGetSavedScoresQualifiersLobby = async (player_id: number): Promise<QualifiersScore[]> => {
    return await executeSqlQuery<QualifiersScore>(`
        SELECT s.* FROM qualifiers_scores s
        WHERE s.player_id = ?;
    `, [player_id]);
};

export const dbUpdateCurrentMapQualifiersLobby = async (lobby_id: number, map_uid: number): Promise<void> => {
    await executeSqlQuery(`
        UPDATE qualifiers_lobbies
        SET current_map = ?
        WHERE lobby_id = ?;
    `, [map_uid, lobby_id]);
};

export const dbMarkQualifiersLobbyAsStarted = async (lobby_id: number, mp_link: string, osu_lobby_id: number): Promise<void> => {
    await executeSqlQuery(`
        UPDATE qualifiers_lobbies
        SET osu_lobby_id = ?, mp_link = ?, is_active = TRUE
        WHERE lobby_id = ?;
    `, [osu_lobby_id, mp_link, lobby_id]);
};

export const dbMarkQualifiersLobbyAsClosed = async (lobby_id: number): Promise<void> => {
    await executeSqlQuery(`
        UPDATE qualifiers_lobbies
        SET is_active = FALSE, is_closed = TRUE
        WHERE lobby_id = ?;
    `, [lobby_id]);
};

export const dbAddQualifiersScore = async (lobby_id: number, player_id: number, score: QualifiersScore): Promise<void> => {
    await executeSqlSingleInsert(`
        INSERT INTO qualifiers_scores (lobby_id, player_id, map_uid, score, accuracy, attempt)
        VALUES (?, ?, ?, ?, ?, ?);
    `, [lobby_id, player_id, score.map_uid, score.score, score.accuracy, score.attempt]);
};

export const dbGetRegistrationByUserId = async (user_id: number): Promise<Registration> => {
    return await executeSqlQueryFirstRow<Registration>(`
        SELECT r.*, u.username FROM registrations r
        INNER JOIN doc.users u ON r.user_id = u.user_id
        WHERE r.tourney_id = 7 AND r.user_id = ? AND r.is_removed = FALSE;
    `, [user_id]);
};

export const dbGetHostByUserId = async (user_id: number): Promise<{ user_id: number }> => {
    return await executeSqlQueryFirstRow<{ user_id: number }>(`
        SELECT s.user_id FROM staff_users s
        WHERE s.tourney_id = 7 AND s.user_id = ? AND (s.role_id = 1 OR s.role_id = 2);
    `, [user_id]);
};
