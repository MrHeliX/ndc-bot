export const isDefined = <T>(item: T | undefined | null): item is T => !!item;

export const parseUsernameToIRC = (name: string) => name.replaceAll(" ", "_");

export const modNames = {
    NF: "NoFail",
    HD: "Hidden",
    HR: "HardRock",
    DT: "DoubleTime"
};