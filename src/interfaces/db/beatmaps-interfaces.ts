export type ModBracket = "NM" | "HD" | "HR" | "DT" | "TB";

export interface Mappool {
    id: number,
    tourney_id: number,
    tourney_round: string,
    released: boolean,
    mappack_download: string | null
};

export interface Beatmap {
    uid: number,
    mappool_id: number,
    map_id: number,
    set_id: number,
    title: string,
    artist: string,
    diff_name: string,
    creator_id: number,
    creator_name: string,
    ar: number,
    cs: number,
    od: number,
    hp: number,
    bpm: number,
    total_length: number,
    star_rating: number,
    mod_star_rating: number | null,
    alternative_star_rating: number | null,
    mod_bracket: ModBracket,
    mod_bracket_order: number,
    download_link: string | null
};