export interface IQualifiersLobby {
    lobby_id: number,
    osu_lobby_id: number,
    custom_id: number,
    tourney_id: number,
    lobby_date: string | null,
    user_id: number,
    mp_link: string | null,
    referee: number | null,
    is_active: boolean
};

export interface QualifiersScore {
    uid?: number,
    lobby_id: number,
    player_id: number,
    map_uid: number,
    score: number,
    accuracy: number,
    attempt: number
};