export interface User {
    user_id: number,
    username: string
};

export interface BotCommand {
    user_id: number,
    username: string,
    trigger: string,
    args?: string[],
    channel: string
};

export interface CommandResponse {
    type: "MESSAGE" | "ACTION";
    message: string;
    user: string;
};

export interface Command {
    trigger: string;
    result: (botCommand: BotCommand) => CommandResponse[] | Promise<CommandResponse[]> | void | Promise<void>;
    minArguments?: number;
    maxArguments?: number;
};