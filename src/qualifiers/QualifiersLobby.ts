import { returnMessage } from "../commands/commands";
import { sendResponse } from "../commands/responses-handler";
import { getModsToSelect } from "../commands/results/qualifiers";
import { dbAddQualifiersScore, dbGetBeatmapsInQualifiersPool, dbGetCurrentBeatmapInLobby, dbGetSavedScoresQualifiersLobby, dbMarkQualifiersLobbyAsClosed, dbUpdateCurrentMapQualifiersLobby } from "../helpers/db-queries";
import { sendWebhookMessage } from "../helpers/discord-webhook-helpers";
import { isDefined, modNames, parseUsernameToIRC } from "../helpers/generic-helpers";
import { Beatmap } from "../interfaces/db/beatmaps-interfaces";
import { QualifiersScore } from "../interfaces/db/qualifiers-interfaces";
import { CommandResponse, User } from "../interfaces/generic-interfaces";
import { osuGetMatchData } from "../services/osu/mp-lobby";
import { getApiV2Token } from "../services/snipe-api";
import { getMappoolFromState } from "../state/mappool";

export class QualifiersLobby {
    public db_lobby_id: number;
    public osu_lobby_id: number;
    public player: User;
    public mp_link: string;
    public channel: string;
    public scores: QualifiersScore[];

    private selectedMap: Beatmap;
    private leaveCount: number = 0;
    private rejoinTimer: NodeJS.Timeout;

    private start_request: boolean = false;
    private start_request_map_checked: (v: unknown) => void;
    private start_request_mods_checked: (v: unknown) => void;
    private start_request_player_checked: (v: unknown) => void;

    private ignore_new_commands: boolean = false;
    private requested_settings_timeout: NodeJS.Timeout;

    public in_panic_mode: boolean = false;

    constructor(lobby_id: number, player: User, mp_link: string, db_lobby_id: number) {
        this.osu_lobby_id = lobby_id;
        this.player = player;
        this.mp_link = mp_link;
        this.channel = `#mp_${lobby_id}`;

        this.db_lobby_id = db_lobby_id;
        dbGetCurrentBeatmapInLobby(this.db_lobby_id).then(response => {
            console.log(response);
            this.selectedMap = response;
        });

        this.retrieveInitialScores();
    };

    private resetTempValues(): void {

    };

    private async retrieveInitialScores(): Promise<void> {
        const existingScores = await dbGetSavedScoresQualifiersLobby(this.player.user_id);
        console.log(existingScores);
        this.scores = existingScores;
    };

    private checkIfScoresRetrievalDone(): boolean {
        if (!this.scores)
            sendResponse([
                returnMessage("Ik ben nog bezig om data op te halen; probeer het over een paar seconden nog eens", this.channel)
            ]);

        return !!this.scores;
    };

    public async setLobbySettingsAndSendInvite(): Promise<CommandResponse[]> {
        const inviteResponse = this.invitePlayer();

        return [
            returnMessage(`!mp set 0 3 1`, this.channel),
            ...this.addReferees([2330619]),
            ...inviteResponse
        ];
    };

    private checkLobbySettingsBeforeStarting(): void {
        if (this.in_panic_mode) return;

        this.start_request = true;
        setTimeout(() => {
            sendResponse([
                returnMessage(`!mp settings`, this.channel)
            ]);
        }, 2000);

        this.requested_settings_timeout = setTimeout(() => {
            this.start_request = false;
            this.ignore_new_commands = false;
            sendResponse([
                returnMessage(`Bancho reageert niet :< probeer opnieuw te readyen om de map te starten`, this.channel)
            ]);
        }, 10000);
    };

    public checkIfCorrectMapSelected(message: string): void {
        if (!this.start_request) return;
        clearTimeout(this.requested_settings_timeout);

        const beatmapUrl = message.match("https://osu.ppy.sh/b/[0-9]*");
        const map_id = +(beatmapUrl?.[0]?.split("/").pop());
        if (map_id === this.selectedMap.map_id) {
            this.start_request_map_checked(null);
            console.log("correct map");
        }
        else {
            sendResponse([returnMessage("De verkeerde map is geselecteerd! Selecteer de map opnieuw", this.channel)], this.player.user_id);
            this.start_request = false;
            this.ignore_new_commands = false;
        }
    };

    public checkIfCorrectModsSelected(message: string): void {
        if (!this.start_request) return;
        clearTimeout(this.requested_settings_timeout);

        const selectedMods = message.split("Active mods: ")[1]?.split(",")?.map(m => m?.trim());
        const requiredMods = getModsToSelect(this.selectedMap.mod_bracket);
        if (requiredMods.length !== selectedMods.length || !requiredMods.every(m => selectedMods.includes(modNames[m]))) {
            sendResponse([returnMessage("De mods staan verkeerd ingesteld! Selecteer de map opnieuw", this.channel)], this.player.user_id);
            this.start_request = false;
            this.ignore_new_commands = false;
        }
        else {
            this.start_request_mods_checked(null);
            console.log("correct mods");
        }
    };

    public checkIfCorrectPlayer(message: string): void {
        if (!this.start_request) return;
        clearTimeout(this.requested_settings_timeout);

        if (message.includes("No Map")) {
            sendResponse([
                returnMessage("Download eerst de map voordat je probeert de lobby te starten", this.channel)
            ], this.player.user_id);
            this.start_request = false;
            this.ignore_new_commands = false;
            return;
        }

        if (message.includes("Not Ready")) {
            sendResponse([
                returnMessage("Klik eerst op ready in de lobby voordat je de map probeert te starten", this.channel)
            ], this.player.user_id);
            this.start_request = false;
            this.ignore_new_commands = false;
            return;
        }

        const user_id = +(message.match("https://osu.ppy.sh/u/[0-9]*")?.[0]?.split("/")?.pop());
        if (user_id !== this.player.user_id) {
            sendResponse([
                returnMessage("Wie ben jij en wat doe je in deze lobby die niet voor jou is??", this.channel)
            ], this.player.user_id);
            this.start_request = false;
            this.ignore_new_commands = false;

            return;
        }

        this.start_request_player_checked(null);
    };

    public invitePlayer(): CommandResponse[] {
        return [returnMessage(`!mp invite #${this.player.user_id}`, this.channel)];
    };

    public addReferees(referees: number[]): CommandResponse[] {
        return [returnMessage(`!mp addref ${referees.map(r => `#${r}`).join(",")}`, this.channel)];
    };

    public kickPlayer(player: string): void {
        sendResponse([
            returnMessage(`!mp kick ${player}`, this.channel)
        ]);
    };

    public logLobbyStatus(): CommandResponse[] {
        const mappool = getMappoolFromState();
        const unplayedMaps = mappool.filter(b => !this.scores.some(s => s.map_uid === b.uid));
        if (!unplayedMaps.length) return;

        const unplayedMapsText = unplayedMaps.map(b => `${b.mod_bracket}${b.mod_bracket_order}`).join(", ");
        const fullTextUnplayed = unplayedMaps.length ? `Nog niet gespeeld: ${unplayedMapsText}` : "";

        return [returnMessage(fullTextUnplayed.concat(" | [https://ndc.huismetbenen.nl/2023/rounds/qualifiers/lobbies website]"), this.channel)];
    };

    public async selectMap(beatmap: Beatmap, mods: string): Promise<CommandResponse[]> {
        if (this.ignore_new_commands) return;
        this.ignore_new_commands = true;

        this.resetTempValues();
        if (!this.checkIfScoresRetrievalDone()) return;

        const responses: CommandResponse[] = [
            returnMessage(`!mp mods ${mods}`, this.channel),
            returnMessage(`!mp map ${beatmap.map_id} 0 | Ready up om de map te starten`, this.channel)
        ];

        await dbUpdateCurrentMapQualifiersLobby(beatmap.uid, this.db_lobby_id);
        return responses;
    };

    public onMapChange(beatmap: Beatmap): void {
        sendWebhookMessage(`${this.player.username} (${this.player.user_id}) heeft ${beatmap.mod_bracket}${beatmap.mod_bracket_order} geselecteerd`);
        this.selectedMap = beatmap;
        this.ignore_new_commands = false;
    };

    public async startMap(): Promise<CommandResponse[]> {
        if (this.ignore_new_commands) return;
        this.ignore_new_commands = true;

        this.resetTempValues();
        if (!this.checkIfScoresRetrievalDone() || this.start_request || !this.selectedMap) {
            console.log(`start request: ${this.start_request}`);
            console.log(`selected map: ${this.selectedMap?.map_id}`);
            return;
        };

        const existingScoresForMap = this.scores.filter(s => s.map_uid === this.selectedMap.uid);
        if (existingScoresForMap.length) {
            this.start_request = false;
            this.ignore_new_commands = false;
            sendResponse([
                returnMessage("Je hebt deze map al een keer gespeeld! Kies een andere map", this.channel)
            ]);
            return;
        }

        try {
            this.checkLobbySettingsBeforeStarting();
            console.log("wait");
            await Promise.all([
                new Promise(r => this.start_request_map_checked = r),
                new Promise(r => this.start_request_mods_checked = r),
                new Promise(r => this.start_request_player_checked = r)
            ]);
            console.log("let's go");

            sendResponse([
                returnMessage(`!mp start 10`, this.channel)
            ]);
        }
        catch (error) {
            console.log(error);
        }
    };

    public onMapStart(): void {
        this.start_request = false;
        this.ignore_new_commands = false;

        sendWebhookMessage(`${this.player.username} (${this.player.user_id}) speelt nu ${this.selectedMap.mod_bracket}${this.selectedMap.mod_bracket_order}`);
    };

    public closeLobbyRequest(): CommandResponse[] {
        const mappool = getMappoolFromState();
        const unplayedMaps = mappool.filter(b => !this.scores.some(s => s.map_uid === b.uid));

        if (unplayedMaps.length) {
            sendWebhookMessage(`${this.player.username} (${this.player.user_id}) heeft geprobeerd de lobby te sluiten (geweigerd - nog niet alle maps zijn gespeeld)`);
            return [returnMessage(`Je hebt nog niet alle maps gespeeld! De maps die je nog moet spelen zijn: ${unplayedMaps.map(b => `${b.mod_bracket}${b.mod_bracket_order}`).join(", ")}`, this.channel)];
        }
        else {
            sendWebhookMessage(`${this.player.username} (${this.player.user_id}) heeft geprobeerd de lobby te sluiten (geaccepteerd - alles is gespeeld)`);
            this.closeLobby(10000);
            return [returnMessage(`De lobby wordt over enkele seconden gesloten; bedankt voor het spelen!`, this.channel)];
        }
    };

    private async closeLobby(delay?: number): Promise<void> {
        if (delay)
            await new Promise(r => setTimeout(r, delay));

        sendResponse([
            returnMessage(`!mp close`, this.channel)
        ]);

        await dbMarkQualifiersLobbyAsClosed(this.db_lobby_id);
        sendWebhookMessage(`Qualifiers lobby van ${this.player.username} (${this.player.user_id}) is gesloten`);
    };

    public async checkForNewScores(): Promise<void> {
        const token = await getApiV2Token();
        const lobbyData = await osuGetMatchData(this.osu_lobby_id, token);
        const mappool = await dbGetBeatmapsInQualifiersPool();

        const playedGames = lobbyData.events.filter(e => !!e.game).map(e => e.game);
        const gamesThatCount = playedGames.filter(g => mappool.some(m => m.map_id === g?.beatmap?.id) || !g?.beatmap);

        const unsubmittedMaps = mappool.filter(m => m.map_id < 0);
        const addedMaps: number[] = [];
        const allScores: ((QualifiersScore & { beatmap: Beatmap }) | null)[] = gamesThatCount.map(g => {
            if (!g || !g.end_time || !g.scores?.length) return null;

            const beatmap = mappool.find(m => m.map_id === g.beatmap?.id) ?? unsubmittedMaps.shift();
            if (!beatmap) return null;

            addedMaps.push(beatmap.uid);

            const score = g.scores.find(s => s.user_id === this.player.user_id);
            if (!score?.score) {
                return {
                    lobby_id: this.osu_lobby_id,
                    player_id: this.player.user_id,
                    map_uid: beatmap.uid,
                    score: 0,
                    accuracy: 0,
                    attempt: addedMaps.filter(m => m === beatmap.uid).length,
                    beatmap
                };
            }

            return {
                lobby_id: this.osu_lobby_id,
                player_id: this.player.user_id,
                map_uid: beatmap.uid,
                score: score.score,
                accuracy: score.accuracy * 100,
                attempt: addedMaps.filter(m => m === beatmap.uid).length,
                beatmap
            };
        });

        const definedScores = allScores.filter(isDefined);
        const newScores = definedScores.filter(ds => !this.scores.some(s => s.map_uid == ds.map_uid));
        if (newScores.length) {
            sendResponse(
                newScores.map(s =>
                    returnMessage(`Nieuwe score op ${s.beatmap.mod_bracket}${s.beatmap.mod_bracket_order}: ${s.score.toLocaleString()} | ${s.accuracy.toFixed(2)}%`, this.channel)
                )
            );

            newScores.map(async s => {
                if (s.attempt <= 1)
                    await dbAddQualifiersScore(this.db_lobby_id, this.player.user_id, s);

                sendWebhookMessage(`Nieuwe score van ${this.player.username} (${this.player.user_id}) op ${s.beatmap.mod_bracket}${s.beatmap.mod_bracket_order}: ${s.score.toLocaleString()} | ${s.accuracy.toFixed(2)}% (${s.attempt}e poging)`)
            });
        }

        this.scores = definedScores;

        if (!this.checkIfMapsLeftToPlay()) {
            sendResponse([
                returnMessage(`Je hebt alle maps gespeeld en bent dus klaar met de qualifiers! De lobby wordt binnen enkele seconden gesloten; bedankt voor het spelen!`, this.channel)
            ]);

            this.closeLobby(10000);
        }
    };

    private checkIfMapsLeftToPlay(): boolean {
        const mappool = getMappoolFromState();
        const mapsLeft = mappool.filter(b => !this.scores.filter(s => s.map_uid === b.uid).length);
        return mapsLeft.length > 0;
    };

    public onLobbyJoin(user: User): void {
        if (this.in_panic_mode) return;
        if (user?.user_id === this.player.user_id) {
            if (this.leaveCount === 0) {
                sendResponse([
                    returnMessage(`Welkom in je qualifiers lobby voor de National Dutch Championship! In deze lobby moet je elke map uit de qualifiers pool 1 keer spelen. De volgorde mag je helemaal zelf bepalen. Selecteer een map met het commando "!map [map id]" (bijvoorbeeld !map NM1 of !map NM2)`, this.channel),
                    returnMessage(`Je kan details over je lobby zien op de [https://ndc.huismetbenen.nl/2023/rounds/qualifiers/lobbies website]`, this.channel)
                ]);

                sendWebhookMessage(`${this.player.username} (${this.player.user_id}) heeft de lobby gejoined`);
            }
            else if (this.leaveCount === 1) {
                clearTimeout(this.rejoinTimer);
                this.rejoinTimer = undefined;
                sendResponse([
                    returnMessage(`Welkom terug in je qualifiers lobby! Verlaat deze a.u.b niet meer voordat je klaar bent met de qualifiers, anders zal de lobby direct worden gesloten om misbruik te voorkomen. Je kunt nu gewoon verder gaan met de qualifiers`, this.channel)
                ]);

                sendWebhookMessage(`${this.player.username} (${this.player.user_id}) is weer terug in de lobby`)
            }
            else {
                this.closeLobby();
            }
        }
    };

    public onLobbyLeave(user: User): void {
        if (this.in_panic_mode) return;
        if (user?.user_id === this.player.user_id) {
            sendResponse([
                returnMessage(`!mp abort`, this.channel)
            ]);

            this.leaveCount++;
            if (this.leaveCount === 1) {
                sendResponse([
                    returnMessage(`Je hebt de lobby verlaten maar bent nog niet klaar met qualifiers! Je hebt 2 minuten de tijd om opnieuw te joinen. Gebruik het commando "!invite" als je een nieuwe invite nodig hebt`, parseUsernameToIRC(this.player.username))
                ]);

                sendWebhookMessage(`${this.player.username} (${this.player.user_id}) heeft de lobby verlaten (1e keer) - 2 minuten om weer te joinen`);

                this.rejoinTimer = setTimeout(() => {
                    sendResponse([
                        returnMessage(`Je qualifiers lobby wordt gesloten omdat je niet op tijd hebt gejoined. Neem contact op met de organisatie in de Discord server`, parseUsernameToIRC(this.player.username))
                    ]);

                    sendWebhookMessage(`${this.player.username} (${this.player.user_id}) is niet op tijd terug in de lobby gekomen - lobby wordt gesloten`);

                    this.closeLobby();
                    this.rejoinTimer = undefined;
                }, 120000);
            }
            else {
                sendResponse([
                    returnMessage(`Je hebt meerdere keren de lobby verlaten; daarom wordt deze nu gesloten. Neem contact op met de organisatie in de Discord server`, parseUsernameToIRC(this.player.username))
                ]);

                sendWebhookMessage(`${this.player.username} (${this.player.user_id}) heeft voor de 2e keer de lobby verlaten - lobby wordt gesloten`);

                this.closeLobby();
            }
        }
    };
};