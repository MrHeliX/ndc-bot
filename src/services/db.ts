import { Connection, createConnection, createPool, Pool, ResultSetHeader } from "mysql2/promise";
import fs from "fs";
import { Client } from "ssh2";

let dbConnection: Pool | Connection;

export const initializeDbConnection = async () => {
    const dbServer = {
        host: process.env.SQL_DB_HOST,
        port: +process.env.SQL_DB_PORT,
        user: process.env.SQL_DB_USER,
        password: process.env.SQL_DB_PASSWORD,
        database: process.env.SQL_DB_DATABASE
    };

    if (process.env.CONNECT_THROUGH_SSH === "true") {
        const tunnelConfig = {
            host: process.env.SSH_HOST,
            port: +process.env.SSH_PORT,
            username: process.env.SSH_USER,
            privateKey: fs.readFileSync(process.env.SSH_KEY_PATH)
        };

        dbConnection = await new Promise((resolve, reject) => {
            const sshClient = new Client();
            sshClient.on("ready", () => {
                console.log("ready");

                sshClient.forwardOut(
                    "127.0.0.1",
                    12345,
                    dbServer.host,
                    +dbServer.port,
                    (err, stream) => {
                        if (err) return reject(err);

                        console.log("db ready");
                        if (process.env.ENV === "dev")
                            resolve(createConnection({ ...dbServer, stream }));
                        else
                            resolve(createPool({ ...dbServer, stream }));
                    }
                );
            }).connect(tunnelConfig);
        });
    }
    else {
        dbConnection = process.env.ENV === "dev"
            ? await createConnection(dbServer)
            : await createPool(dbServer);
    }
};

export const executeSqlQuery = async <T>(query: string, params?: any[]): Promise<T[]> => {
    try {
        const [rows] = await dbConnection.query(query, params);
        return rows as T[] || [];
    } catch (error) {
        console.error(error);
    }
};

export const executeSqlQueryFirstRow = async <T>(query: string, params?: any[]): Promise<T> => {
    return (await executeSqlQuery<T>(query, params))[0];
};

export const executeSqlInsert = async (query: string, params?: any[]): Promise<ResultSetHeader[]> => {
    try {
        return await dbConnection.query(query, params) as ResultSetHeader[];
    } catch (error) {
        console.error(error);
    }
};

export const executeSqlSingleInsert = async (query: string, params?: any[]): Promise<ResultSetHeader> => {
    return await (await executeSqlInsert(query, params)).filter(r => !!r)[0];
};