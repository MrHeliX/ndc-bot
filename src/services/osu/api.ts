import needle from "needle";

export const osuApiV1Request = async<T>(endpoint: string, query: string): Promise<T | null> => {
    return new Promise(resolve => {
        const url = `https://osu.ppy.sh/api/${endpoint}?k=${process.env.OSU_API_KEY}&${query}`;
        needle.get(url, { json: true }, (error, response) => {
            if (!error && response?.statusCode === 200)
                resolve(response.body);
            else {
                resolve(null);
            }
        });
    });
};

export const osuApiV2Request = async<T>(endpoint: string, access_token: string): Promise<T | null> => {
    return new Promise(resolve => {
        needle.get(`https://osu.ppy.sh/api/v2/${endpoint}`, {
            headers: { "Authorization": `Bearer ${access_token}` },
            json: true
        }, async (error, response) => {
            if (response?.statusCode === 401 || response?.statusCode === 403) {
                console.error("Invalid token", access_token);
            }

            else if (response?.statusCode === 404)
                resolve(null);

            else {
                if (error) console.error(error);
                else {
                    resolve(response.body);
                    return;
                }
            }

            resolve(null);
        });
    });
};