import { OsuApiV1User } from "../../interfaces/osu/users";
import { addUserToState } from "../../state/users";
import { executeSqlQuery } from "../db";
import { osuApiV1Request } from "./api";

export const osuGetPlayer = async (username: string): Promise<OsuApiV1User> => {
    const response = await osuApiV1Request<OsuApiV1User[]>("get_user", `u=${username}&type=string`);
    if (response?.length && response[0]?.user_id) {
        const osuPlayer = response[0];
        addUserToState(username, +osuPlayer.user_id);

        await executeSqlQuery(`
            UPDATE doc.users SET username = ? WHERE user_id = ?;
        `, [osuPlayer.username, +osuPlayer.user_id]);

        return osuPlayer;
    }
};