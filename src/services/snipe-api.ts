import needle from "needle";
import { executeSqlQuery, executeSqlQueryFirstRow } from "./db";

const getFromSnipeApi = <T>(endpoint: string, parameters: object = {}): Promise<T | null> => {
    return new Promise(resolve => {
        const url = `https://api.huismetbenen.nl/${endpoint}`;
        const query = Object.keys(parameters ?? {}).length > 0
            ? Object.keys(parameters).reduce((total, current, index) => {
                if (index !== 0) total += "&";
                total += `${current}=${parameters[current]}`;
                return total;
            }, "?")
            : "";

        needle.get(url + query, { headers: { Authorization: `Bearer ${process.env.SNIPE_API_KEY}` }, json: true }, async (err, response) => {
            try {
                if (err || response.statusCode !== 200) {
                    resolve(null);
                }
                resolve(response.body);
            } catch (error) {
                resolve(null);
            }
        });
    });
};

export const getApiV2Token = async (): Promise<string> => {
    const res = await executeSqlQueryFirstRow<{ user_id: number, token: string }>(`
        SELECT * FROM staff_sessions
        WHERE user_id = ?;
    `, [+process.env.IRC_USERID]);

    return res.token;
};

export interface CountryLeader {
    id: string,
    name: string,
    country: string,
    is_valid: boolean,
    osu_id: number,
    access_token: string,
    refresh_token: string
};