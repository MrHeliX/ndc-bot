import { Client } from "irc-framework";
import { Beatmap } from "../interfaces/db/beatmaps-interfaces";
import { User } from "../interfaces/generic-interfaces";
import { QualifiersLobby } from "../qualifiers/QualifiersLobby";

export type IState = {
    client: Client,
    users: User[],
    lobbies: QualifiersLobby[],
    mappool: Beatmap[],
    busyUsers: { [key: string]: boolean }
};

const state: IState = {
    client: null,
    users: [],
    lobbies: [],
    mappool: [],
    busyUsers: {}
};

export const getStateValue = <K extends keyof IState>(key: K): IState[K] => {
    return { ...state }[key];
};

export const setStateValue = <K extends keyof IState>(key: K, value: IState[K]) => {
    state[key] = value;
};

export const setUserAsBusy = (username: string) => {
    console.log(`NOW BUSY: ${username}`);
    state.busyUsers[username] = true;
};

export const setUserAsAvailable = (username: string) => {
    state.busyUsers[username] = false;
};

export const isUserBusy = (username: string): boolean => !!state.busyUsers[username];