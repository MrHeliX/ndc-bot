import { BotCommand, User } from "../interfaces/generic-interfaces";
import { getStateValue, setStateValue } from "./state";

export const addUserToState = (username: string, user_id: number): void => {
    const users = getStateValue("users");
    if (!users.some(u => u.user_id === user_id))
        setStateValue("users", [...users, { user_id, username }]);
};

export const getUserFromStateByName = (username: string): User | undefined => {
    return getStateValue("users").find(u => u.username === username.replaceAll(" ", "_"));
};

export const getUserFromStateById = (user_id: number): User | undefined => {
    return getStateValue("users").find(u => u.user_id === user_id);
};